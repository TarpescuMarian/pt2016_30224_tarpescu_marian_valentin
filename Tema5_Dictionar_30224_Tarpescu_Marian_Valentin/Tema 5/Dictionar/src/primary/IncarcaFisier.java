package primary;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class IncarcaFisier {
	
	String nume ="";
	File fisier;
	FileInputStream fis = null;
	BufferedInputStream buf = null;
	DataInputStream dat = null;
	BufferedWriter bufw = null;
	
	
	public IncarcaFisier(String name)
	{
		assert name!=null : "Fisierul nu exista";
		try
		{
			nume=name;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		assert nume!="";
	}
	
	public void citire()
	{
		assert nume!="" : "Nume fisier Incorect !";
		try
		{
			  fisier= new File(nume);
		      fis = new FileInputStream(fisier);
		      buf = new BufferedInputStream(fis);
		      dat = new DataInputStream(buf);   
			  bufw = new BufferedWriter(new FileWriter(fisier,true));
			
		}
		
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public String citireLinie()
	{
		try
		{
			if(dat.available()!=0)
			{
				return dat.readLine();
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return "";
	}
	
	String findKey(String linie)
	{
		assert (linie!=""): "Linie goala";
		char[] c=linie.toCharArray();
		String rezultat="";
		int i=0;
		while(c[i]!=' ')
		{
			rezultat+=String.valueOf(c[i]);
			i++;
		}
		return rezultat;
	}
	
	ArrayList <String> findDef(String linie)
	{
		assert linie!=null: "Linie goala";
		ArrayList <String> lista = new ArrayList<String>();
		String rezultat="";
		char[] c=linie.toCharArray();
		int i=0;
		while(c[i]!=' ')
		{
			i++;
		}
		while(c[i]!=',')
		{
			rezultat=rezultat+String.valueOf(c[i]);
			i++;
		}
		lista.add(rezultat);
		return lista;
	}
	
	
	public void scriereLinie(String text)
	{
		 assert text!=null : "Operatia de scriere nu poate fi efectuata";
		    try 
		    {
		        BufferedWriter out = new BufferedWriter(new FileWriter("Dictionar.txt", true));
		        out.newLine();
				out.write(text);
		        out.close();
		    } 
			catch (AssertionError e) 
			{ 
				e.printStackTrace();
		  	}    
			catch (IOException e) 
			{ 
				e.printStackTrace();
		  	}    
	}
	
	public String stergereLinie(String file, String linie)
	{
		try
		{
			File fis1= new File(file);
			if(!fis1.isFile())
			{
				return "Eroare la parametri introdusi";
			}
			File tempFis=new File(fis1.getAbsolutePath()+".tmp");
		    BufferedReader buf1 = new BufferedReader(new FileReader(file));
		    PrintWriter pw = new PrintWriter(new FileWriter(tempFis));    
		    String line = null;
		    while ((line = buf1.readLine()) != null) 
			{
		    	if (!line.trim().equals(line)) 
				{
		          pw.println(line);
		          pw.flush();
		        }
		    }
		    
		    pw.close();
		    buf1.close();
		    if (!fis1.delete()) 
			{
		    	return "Operatia de stergere nu poate fi efectuata";
		    }
		    
		    if (!tempFis.renameTo(fis1))
				  return "Operatia de redenumire nu poate fi efectuata";
		    }
		
		    catch (IOException e) 
			{
		    	e.printStackTrace();
		    }
		
			return "Eliminare reusita";
	}
	
	
	public void inchidereFisier() 
	{
	 try 
	 {
		 	fis.close();
		 	buf.close();
			dat.close();
	 }
	 
	 catch (IOException e) 
	 {
		 	e.printStackTrace(); 
	 }
	}

}
