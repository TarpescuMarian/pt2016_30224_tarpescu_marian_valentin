package primary;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

public class Dictionar {

	Map <String, ArrayList<String>> lista_cuvinte;
	
	
	public Dictionar()
	{
		this.lista_cuvinte=new Hashtable<String, ArrayList<String>>();
	}
	
	
	public ArrayList<String> get(String key)
	{
		return lista_cuvinte.get(key);
	}
	
	
	public boolean validareC(String str)
	{
		assert str!=null:"Cautare eronata !";
		int i=0;
		char[] c=str.toLowerCase().toCharArray();
		while(i<c.length)
		{
			if(c[i]=='*')
				break;
			if(c[i]<'a'|| c[i]>'z')
				return false;
			i++;
		}
		return true;
	}
	
	
	public boolean validareA(String str)
	{
		assert str!=null: "Adaugare eronata !";
		int i=0;
		char[] c=str.toLowerCase().toCharArray();
		while(i<c.length)
		{
			if(c[i]<'a'|| c[i]>'z')
				return false;
			i++;
		}
		return true;
	}
	
	
	public String getDefinitie(String key)
	{
		try
		{
			ArrayList lista=lista_cuvinte.get(key);
			String definitie = "";
			for(int i=0; i<=lista.size();i++)
			{
				definitie=definitie + lista.get(i);
			}
			return definitie;
		}
		
		catch(Exception e)
		{
			return "Nici o definitie viabila";
		}
	}
	
	
	public boolean cautaCheie(String str)
	{
		try
		{
			return (lista_cuvinte.containsKey(str));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
			
		}
	}
	
	
	public void definitieNoua(String key, ArrayList<String> def)
	{
		assert key!=null && (!cautaCheie(key)) : "Cuvantul exista";
		int temp = lista_cuvinte.size();
		lista_cuvinte.put(key, def);
		assert lista_cuvinte.size()==temp+1;
	}
	
	
	public boolean elimina(String str)
	{
		try
		{
			assert lista_cuvinte.containsKey(str);
			assert lista_cuvinte.size()>0;
			lista_cuvinte.remove(str);
		}
		catch (Exception e)
		{
			return false;
		}
		return true;
	}
	
	
	public boolean match(String str1, String str2)
	{
		return Pattern.matches(str1, str2);
	}
	
	
	public String[] patternMatch(String str)
	{
		String[] s=new String[20];
		s[0]=".";
		int i=0;
		
		try
		{
			Iterator it = lista_cuvinte.keySet().iterator();
			while(it.hasNext())
			{
				Object key = it.next();
				String s1= (String) key;
				if (match(str, s1))
				{
					s[i]=s1;
					i++;
				}
			}
			return s;
		}
		
		catch(Exception e)
		{
			return s;
		}
	}
	
	public boolean isMatch(String str1, String str2)
	{
		assert (str1!=null)&&(str2!=null):"Potrivirea a esuat";
		if(str2.compareTo("*")==0)
			return true;
		String str11, str22;
		str11=((String)str1).toLowerCase();
		str22=((String)str2).toLowerCase();
		
		if(str22.length()>str11.length())
			return false;
		
		if(str22.indexOf("*")!=-1)
		{
			if(str22.indexOf("*")+1>str11.length())
				return false;
			
			int i=0;
			while(str22.charAt(i)!='*')
			{
				if(str22.charAt(i)!='?')
					if(str22.charAt(i)!=str11.charAt(i))
						return false;
				i++;
			}
			return true;
		}
		
		if(str22.length()==str11.length())
		{
			for(int i=0;i<str22.length();i++)
			{
				if(str22.charAt(i)!='?')
					if(str22.charAt(i)!=str11.charAt(i))
						return false;
			}
			return true;
		}
		else
			return false;
	}
	public String[] gasire(String str)
	{
		String[] s=new String[20];
		int i=0;
		try
		{
			Iterator it=lista_cuvinte.keySet().iterator();
			while(it.hasNext())
			{
				Object key=it.next();
				String s1=(String)key;
				if(isMatch(s1,str))
				{
					s[i]=s1;
					i++;
				}
			}
			return s;
		}
		catch(Exception e)
		{
			return s;
		}
	}
	
	
	public boolean verifConsist() 
	{
		assert lista_cuvinte!=null : "Nu exista dictionar";
		try 
		{
			Iterator it = lista_cuvinte.keySet().iterator();	
			String rezultat="";
			while (it.hasNext())
	 		{
	 		   Object key = it.next();
	 		   ArrayList<String> list=lista_cuvinte.get(key);
			   for (int i = 0; i<list.size(); i++) 
			   {
					rezultat = rezultat + list.get(i);
			   }			   	
			   String[] rez=rezultat.split(" ");   
			   for (int i=0;i<rez.length;i++) {
				    if ((rez[i].length()>2)&&(!(cautaCheie(rez[i]))))
					   		return false;	  		   
			   }					   							
			   rezultat="";
	 		}
			return true;
		}
		catch(Exception e)
		{		
			return false;
		}	
		catch(AssertionError e) 
		{
			return false;
		}
		
	}
	
	
	public void getDictionar()
	{
		try
		{
			assert lista_cuvinte!=null : "Operatie imposibila";
			IncarcaFisier aux = new IncarcaFisier("Dictionar.txt");
			ArrayList aux2=new ArrayList<String>();
			aux.citire();
			String str =aux.citireLinie();
			while(str!="")
			{
				definitieNoua(aux.findKey(str), aux.findDef(str));
				aux2=aux.findDef(str);
				String rezultat ="";
				for(int i=0;i<aux2.size();i++)
				{
					rezultat=rezultat+ aux2.get(i);
				}
				str=aux.citireLinie();
			}
			aux.inchidereFisier();
			assert lista_cuvinte.size()>0 : "Fisierul este gol !";
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
}
