package pack_polinoame;

import static org.junit.Assert.*;

public class Test {

	@SuppressWarnings("deprecation")
	@org.junit.Test
	public void Polinom() 
	{
		Polinom junit = new Polinom("7x^3 + 5x^2 + 2x^1 + 2");
		double rezultat = junit.getCoeficient(0);
		assertEquals(2, rezultat);
	}

}
