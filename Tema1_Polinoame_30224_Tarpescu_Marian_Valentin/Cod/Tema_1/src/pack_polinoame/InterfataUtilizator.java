package pack_polinoame;
import java.awt.Font;
import java.awt.event.*;
import javax.swing.*;
import java.text.DecimalFormat;
import java.lang.Double;
public class InterfataUtilizator extends JFrame implements ActionListener,KeyListener {
	
		
		private static final long serialVersionUID = 1L;
		private JButton aduna=new JButton("+");
		private JButton minus=new JButton("-");
		private JButton mul=new JButton("*");
		private JButton div=new JButton("/");
		private JButton deriv=new JButton("Deriveaza");
		private JButton integ=new JButton("Integreaza");
		private JButton citeste=new JButton("CITESTE");
		
		
		private JTextField first_polinom= new JTextField ();
		private JTextField second_polinom= new JTextField ();
		private JTextField txt_rezultat= new JTextField();
		private JLabel txt_rest=new JLabel();
		
		private JLabel first_pol= new JLabel("Primul polinom: ");
		private JLabel second_pol= new JLabel("Al doilea polinom: ");
		private JLabel rezultat_label= new JLabel("Rezultat ");
		private JLabel exemplu_label= new JLabel("Forma Polinomului ");
		private JLabel ex_label= new JLabel("5x^4 - 2x^3 + x^2 - x^1 + 10");
		
		private JPanel panou = new JPanel();
		private JLabel label_eroare= new JLabel();
		
		private Polinom polinom_1;
		private Polinom polinom_2;
		private Operatii rezultat;
	
		public InterfataUtilizator ()
		{
			panou.setLayout(null);
			aduna.setBounds(10, 90, 50, 40);
			minus.setBounds(90, 90, 50, 40);
			mul.setBounds(10, 155, 50, 40);
			div.setBounds(90, 155, 50, 40);
			deriv.setBounds(10, 231, 130, 30);
			integ.setBounds(10, 291, 130, 30);
			citeste.setBounds(202, 154, 200, 40);
			first_polinom.setBounds(202, 23, 202, 40);
			second_polinom.setBounds(202, 90, 202, 40);
			txt_rezultat.setBounds(202, 227, 202, 40);
			txt_rest.setBounds(202, 287, 202, 40);
			first_pol.setBounds(212, 0, 113, 25);
			second_pol.setBounds(212, 67, 130, 25);
			label_eroare.setBounds(10, 350, 414, 33);
			rezultat_label.setBounds(208, 202, 130, 25);
			exemplu_label.setBounds(38,24,147,14);
			ex_label.setBounds(10,49,197,14);
			
			
			Font font_operatii=new java.awt.Font("BankGothic Md BT", Font.BOLD, 18);
			Font font_derivare_integrare=new java.awt.Font("BankGothic Md BT", Font.BOLD, 13);
			Font font_polinoame=new java.awt.Font("Arial Narrow", Font.PLAIN, 16);
			Font font_citeste=new java.awt.Font("BankGothic Md BT", Font.BOLD, 19);
			Font font_eroare=new java.awt.Font("Arial Narrow", Font.PLAIN, 11);
			Font font_label=new java.awt.Font("Tahoma", Font.PLAIN, 12);
			
			aduna.setFont(font_operatii);
			minus.setFont(font_operatii);
			mul.setFont(font_operatii);
			div.setFont(font_operatii);
			integ.setFont(font_derivare_integrare);
			deriv.setFont(font_derivare_integrare);
			citeste.setFont(font_citeste);
			label_eroare.setFont(font_eroare);
			first_polinom.setFont(font_polinoame);
			second_polinom.setFont(font_polinoame);
			txt_rezultat.setFont(font_polinoame);
			txt_rest.setFont(font_polinoame);
			first_pol.setFont(font_label);
			second_pol.setFont(font_label);
			rezultat_label.setFont(font_label);
			ex_label.setFont(font_polinoame);
			exemplu_label.setFont(font_label);
			
			panou.add(aduna);
			panou.add(minus);
			panou.add(mul);
			panou.add(div);
			panou.add(integ);
			panou.add(deriv);
			panou.add(citeste);
			panou.add(label_eroare);
			panou.add(first_polinom);
			panou.add(second_polinom);
			panou.add(txt_rezultat);
			panou.add(txt_rest);
			panou.add(first_pol);
			panou.add(second_pol);
			panou.add(rezultat_label);
			panou.add(exemplu_label);
			panou.add(ex_label);
			
			
			aduna.addActionListener(this);
			minus.addActionListener(this);
			mul.addActionListener(this);
			div.addActionListener(this);
			integ.addActionListener(this);
			deriv.addActionListener(this);
			citeste.addActionListener(this);
			first_polinom.addKeyListener(this);
			second_polinom.addKeyListener(this);
			
			this.add(panou);
			this.setSize(450,435);
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			this.setVisible(true);
			this.setTitle("Sistem de procesare a polinoamelor");
		}
		
		
		
		public void keyTyped(KeyEvent eveniment) 
		 {
		    txt_rezultat.setText("");
			label_eroare.setText("	Apasati butonul Citeste inainte de a efectua operatiile. ");
		}
		
		
		public void keyPressed(KeyEvent eveniment)
		{	
			
		}
		
		
		public void keyReleased(KeyEvent eveniment) 
		{	
			
		}
		    
			
		public String afisare(Polinom afis)
		{	
			String str=new String();
			DecimalFormat f=new DecimalFormat("#.##");
				
			if(Double.isInfinite(afis.getCoeficient(afis.getGrad())) || Double.isNaN(afis.getCoeficient(afis.getGrad())))
				{	
					label_eroare.setText("Nu se poate realiza impartirea la 0");
					txt_rezultat.setText("");
					txt_rest.setText("");
				}
			else
				{	
					for(int i=afis.getGrad()+2; i>=0; i--)
						{
							if(afis.getCoeficient(i)!= 0) 	
								{	
									if(i==0)
										str+=""+f.format(afis.getCoeficient(i));
									else
									{	
										if(afis.getCoeficient(i)==1)
										{	
											str+="x^"+i+" + "; 
										}
										else
										{   
											str+=f.format(afis.getCoeficient(i))+"x^"+i+" + ";	
										}
									} 
								}
						}
				}
			str+=" ";
			str=str.replaceAll("-","-\\ ");
			str=str.replaceAll("\\+\\ -", "-");
			str=str.replaceAll("\\+\\ \\ ","");
			if(str.equals(" "))
				{
					str=""+0;
				}
			return str;
		}
				
				
		
		    
		public void actionPerformed(ActionEvent eveniment1)
		{
			Object val=eveniment1.getSource();
			try{
				if(val == citeste)
				{
					polinom_1=new Polinom (first_polinom.getText());
					polinom_2=new Polinom (second_polinom.getText());
					rezultat = new Operatii(polinom_1,polinom_2);
					label_eroare.setText(""); 
				}
						
				 if(val == aduna)
				 {  	 
				 	txt_rezultat.setText(afisare(rezultat.add()));
				 	label_eroare.setText("S-a efectuat adunarea!");
				 	
				 }
				 
				 if(val == minus)
				 {  
					 txt_rezultat.setText(afisare(rezultat.minus()));
					 label_eroare.setText("S-a efectuat scaderea!");
				 }
				 
				 if(val == mul)
				 {   
					 txt_rezultat.setText(afisare(rezultat.multiply()));
				     label_eroare.setText("S-a efectuat inmultirea!");
				 }
				 
				 if(val == div)
				 {   label_eroare.setText("S-a efectuat impartirea!");
				 	 txt_rezultat.setText(afisare(rezultat.divide()));
					 txt_rest.setText(afisare(rezultat.getRest()));
					
					 
					 
				 }
				 if(val == deriv )
				 {
					  txt_rezultat.setText(afisare(rezultat.differentiate()));
					  label_eroare.setText("S-a efectuat derivarea!");
				 
				 }
				 if(val == integ)
				 {
					 txt_rezultat.setText(afisare(rezultat.integrate()));
					 label_eroare.setText("S-a efectuat integrarea!");
				 
				 }
		}
			
		 catch(NumberFormatException f)
		{ 
			 label_eroare.setText("Introduceti polinoamele corect");
					
		}
			
		catch(NullPointerException f)
		 {   
					 
			 label_eroare.setText("Introduceti polinoamele");
					 
		 }
}
		
				
		public static void main(String[] args)
		{
			new InterfataUtilizator();
		}
		
}





