package pack_polinoame;

public class Operatii {
	
	private Polinom first;
	private Polinom second;
	private Polinom rest;
	
	public Operatii(Polinom first, Polinom second)
	{
		this.first=first;
		this.second=second;
	}
	
	public Polinom add()
	{
		int gradMaxim,i;
		if(first.getGrad() > second.getGrad())
			gradMaxim=first.getGrad();
		else
			gradMaxim=second.getGrad();
		
		Polinom rezultat = new Polinom(gradMaxim);
		for(i=0; i<=gradMaxim; i++)
		{
			rezultat.setCoeficient(i, first.getCoeficient(i) + second.getCoeficient(i));
			
		}
		return rezultat;
				
	}
	
	public Polinom minus()
	{
		int gradMaxim,i;
		if(first.getGrad() > second.getGrad())
			gradMaxim=first.getGrad();
		else
			gradMaxim=second.getGrad();
		
		Polinom rezultat = new Polinom(gradMaxim);
		for(i=0; i<=gradMaxim; i++)
		{
			rezultat.setCoeficient(i, first.getCoeficient(i) - second.getCoeficient(i));
			
		}
		return rezultat;
				
	}
	
	public Polinom multiply()
	{
		int grad,i,j;
		grad=first.getGrad() + second.getGrad();
		Polinom rezultat = new Polinom(grad);
		
		for(i=0; i<=first.getGrad(); i++)
		{
			for(j=0; j<=second.getGrad(); j++)
			{
				rezultat.setCoeficient(i+j, first.getCoeficient(i)*second.getCoeficient(j));
			}
		}
		return rezultat;
	}
	
	
	public Polinom divide()
	{
		int grad,i,q,w;
		double e;
		if(first.getGrad() > second.getGrad())
			grad=first.getGrad();
		else
			grad=0;
		
		Polinom rezultat = new Polinom(grad);
		rest = new Polinom(first.getGrad());
		for(i=0; i<first.getGrad(); i++)
		{
			rest.setCoeficient(i, first.getCoeficient(i));
		}
		q=rest.getGrad();
		w=second.getGrad();
		while(q>=w)
		{
			e=rest.getCoeficient(q)/second.getCoeficient(w);
			rezultat.setCoeficient(q-w, e);
			
			for(i=0; i<=w; i++)
					rest.setCoeficient(q-w+i, -e*second.getCoeficient(i));
			q--;
		}
		return rezultat;
	}
	
	
	public Polinom getRest()
	{
		return rest;
	}
	
	
	public Polinom differentiate()
	{
		int i;
		Polinom rezultat=new Polinom(first.getGrad());
		for(i=0; i<=first.getGrad(); i++)
		{
			rezultat.setCoeficient(i, first.getCoeficient(i+1)*(i+1));
		}
		return rezultat;
		
	}
	
	public Polinom integrate()
	{
		int i;
		Polinom rezultat=new Polinom(first.getGrad());
		for(i=1; i<=first.getGrad()+1; i++)
		{
			rezultat.setCoeficient(i, first.getCoeficient(i-1) / i);
		}
		return rezultat;
		
	}

}
