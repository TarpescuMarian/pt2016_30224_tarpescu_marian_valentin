package pack_polinoame;

public class Polinom {

	private int grad;
	private double coeficient[]=new double[200];
	
	
	public Polinom (String expresie)
	{
		int grad_maxim=0,primul,second,lungime,i;
		expresie=expresie.replaceAll(" ","");
		expresie=expresie.replaceAll("\\-","\\+-");
		
		if(expresie.equals(""))
		{
			expresie="0";
		}
		
		String[] nou = expresie.split("\\+");
		
		lungime=nou.length;
		
		for(i=0; i<lungime; i++)
		{
			String[] coef = nou[i].split("x\\^");
			if(coef[0].equals(""))
			{
				primul=1;
				if(i==0)
					primul=0;
				
			}
			else
			{
				if(coef[0].equals("-"))
					primul=-1;
				else
					primul=Integer.parseInt(coef[0]);
	
			}
			try
			{
				second=Integer.parseInt(coef[1]);
			}
			catch(ArrayIndexOutOfBoundsException e)
			{
				second=0;
			}
			this.coeficient[second]+=primul;
			
			if(second>grad_maxim)
			{
				grad_maxim=second;
			}
		
		}
	this.grad=grad_maxim;
	
}
	
	
	public Polinom (int grad)
	{
		this.grad=grad;
	}
	
	public int getGrad()
	{
		return grad;
	}
	
	
	
	public void setGrad(int grad)
	{
		this.grad=grad;
		
	}
	
	
	
	public double getCoeficient(int index)
	{
		return coeficient[index];
	}
	
	
	
	public void setCoeficient(int index, double c)
	{
		this.coeficient[index]+=c;
	}
}
