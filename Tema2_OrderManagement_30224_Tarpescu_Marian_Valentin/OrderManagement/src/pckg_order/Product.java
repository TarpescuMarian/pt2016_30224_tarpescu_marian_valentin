package pckg_order;

public class Product implements Comparable<Product>{

	private int id;
	private int nr;
	private int pret;
	private String denumire;
	
	public Product(int id, int nr, int pret, String denumire)
	{
		this.id=id;
		this.nr=nr;
		this.pret=pret;
		this.denumire=denumire;
	}
	
	protected boolean Verificare()
	{
		boolean a=true;
		
		if(id<0)
			a=false;
		if(nr<0)
			a=false;
		if(pret<0);
			a=false;
		if(denumire.equals(""))
			a=false;
		
		return a;
	}
	
	/** obtine codul produsului
     * @post @nochange
     * @post @result ==id
     */
	public int getId()
	{
		return this.id;
	}
	
	
	/**seteaza codul produsului
     * @param c
     * @invariant pret,nr,denumire
     */
	public void setCod(int c)
	{
		this.id=c;
	}
	
	
    /** obtine nr de produse
    * @post @nochange
    * @post @result ==nr
    */
	public int getNr()
	{
		return this.nr;
	}
	
	
    /**seteaza numarul de produse
    * @param n
    * @invariant pret,id,denumire
    */
	public void setNr(int n)
	{
		this.nr=n;
	}
	
	
	 /**obtine pretul unui produs
     * @post @nochange
     * @post @result==pret
     */
	public int getPret()
	{
		return this.pret;
	}
	
	
    /**seteaza pretul unui produs
     * @param pret
     * @invariant id,nr,denumire
     */
	public void setPret(int pret)
	{
		this.pret=pret;
	}
	
	
    /**obtine denumirea unui produs
     * @post @nochange
     * @post @result==denumire
     */
	public String getDenumire()
	{
		return this.denumire;
	}
	
	
    /**seteaza denumirea unui produs
     * @param nume
     * @invariant id,nr,pret
     */
	public void setDenumire(String nume)
	{
		this.denumire=nume;
	}
	
	
    /**creste numarul/cantitatea produsului
   * @param n
   * @invariant pret,id,denumire
   */
	public void add(int n)
	{
		this.nr=this.nr+n;
	}
	
	
    /**scade din cantitaea  produsului
     * @param n
     * @return 
     * @invariant pret,id,denumire
     */
	public void minus(int n)
	{
		this.nr=this.nr-n;
	}
	
	
	/**
	 * se compara cu ajutorul denumirii produsului
	 * @param p
	 * @invariant pret,id,nr
	 */
	public int compareTo(Product p)
	{
		return denumire.compareTo(p.getDenumire());
	}
}
