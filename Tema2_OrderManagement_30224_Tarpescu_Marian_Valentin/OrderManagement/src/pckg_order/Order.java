package pckg_order;

public class Order implements Comparable <Order>{
	
	private int id;
	private String denumireProdus;
	private int cantitate;
	
	public Order (int id, int cantitate, String denumireProdus)
	{
		this.id=id;
		this.cantitate=cantitate;
		this.denumireProdus=denumireProdus;
		
		assert isWellFormed();
	}
	
	protected boolean isWellFormed()
	{
		boolean stare=true;
		if(id<0)
			stare=false;
		if(cantitate<0)
			stare=false;
		return stare;
	}
	
	
    /** returneaza id-ul
	* @post @nochange
    * @post @result ==id
	*/
	public int getId()
	{
		return this.id;
	}
	
	
    /**seteaza id-ul unui produs
    * @param c
	* @invariant cantitate, denumireProdus
	*/
	public void setID(int c)
	{
		this.id=c;
	}
	
	
    /** returneaza cantitatea
	* @post @nochange
    * @post @result ==cantitate
	*/
	public int getCantitate()
	{
		return this.cantitate;
	}
	
	
    /**seteaza id unui produs
    * @param cant
	* @invariant id,denumireProdus
	*/
	public void setCantitate(int cant)
 	{
		this.cantitate = cant;
	}
	
	
    /** returneaza numele unui produs
	* @post @nochange
    * @post @result ==denumireProdus
	*/
	public String getDenumire()
	{
		return this.denumireProdus;
	}
	
	
    /** seteaza numele unui produs
	* @post @nochange
	* @invariant id,cantitate
	*/
	public void setDenumire(String nume)
	{
		this.denumireProdus=nume;
	}
	
	
	/**
	 * compara doua produse folosind id-ul lor
	 */
	public int compareTo(Order ord)
	{
		return ((Integer)this.id).compareTo(ord.getId());
	}
}
