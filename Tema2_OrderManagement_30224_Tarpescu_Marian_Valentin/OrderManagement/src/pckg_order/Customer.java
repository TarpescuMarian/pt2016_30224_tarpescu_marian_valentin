package pckg_order;

public class Customer {
	@SuppressWarnings("unused")
	private String nume;
	@SuppressWarnings("unused")
	private int cod;
	private Order order;
	
	public Customer(String nume,int cod, String denumire, int cant)
	{
		this.nume=nume;
		this.cod=cod;
		order=new Order(cod,cant,denumire);
	}
	
	/**Metoda de obtinere a comenzii
	 * 
	 * @post @nochange
	 * @post @result==comanda
	 */
	
	public Order takeOrd()
	{
		return order;
	}

}
