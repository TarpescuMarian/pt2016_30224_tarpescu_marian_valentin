package pckg_order;

import java.util.*;
import javax.swing.*;

public class Warehouse {
	
	private TreeSet<Product> p =new TreeSet<Product>();
	private Product p0, p1, p2, p3, p4, p5, p6;
	private String mesaj;
	@SuppressWarnings("unused")
	private int size;
	
	public Warehouse()
	{
		p0=new Product(0, 1120, 2, "Cuie");
		p.add(p0);
		p1=new Product(1, 223, 20, "Ghiveci");
		p.add(p1);
		p2=new Product(2, 460, 14, "Scoci");
		p.add(p2);
		p3=new Product(3, 356, 10, "Tevi");
		p.add(p3);
		p4=new Product(4, 858, 6, "Suruburi");
		p.add(p4);
		p5=new Product(5, 460, 8, "Sipci");
		p.add(p5);
		p6=new Product(6, 99, 18, "Patent");
		p.add(p6);
	}
	
	public boolean isWellFormed()
	{
		boolean a=true;
		p0=p.first();
		for(Iterator<Product> it=p.iterator(); it.hasNext();)
		{
			p1=it.next();
			if(p0.compareTo(p1)>0)
				a=false;
			p0=p1;
		}
		return a;
	}
	
	
    /** returneaza un mesaj
	* @post @nochange
    * @post @result ==mesaj
	*/
	public String getMesaj()
	{
		return mesaj;
	}
	
	
    /** seteaza un anumit mesaj
    * @param m
	* @post @nochange
	*/
	public void setMesaj(String m)
	{
		this.mesaj=m;
	}
	
	
    /** se afiseaza produsele din depozit
    * @param zona
	* @post @nochange
	*/
    public void raporDep(JTextArea zona) {
        for (Product i : p)
            zona.append("Produs: "+i.getDenumire()+"   Id: "+i.getId()+"   Pret: "+i.getPret()+"   Stoc: "+i.getNr()+"\n");
    }
	
    
    /** returneaza valoarea totala a depozitului
	* @post @nochange
	*/
    public int valoareTotala() {
        int aux=0;
        for (Product i : p)
            aux=aux+i.getPret()*i.getNr();
        return aux;
    }
    
    
    /** cauta un produs dupa nume
	* @param n
	* @pre prod!=null;
	* @post @nochange
	*/
    public Product findProdus(String n) {
        assert n!=null: "Prametri gresiti";
        Product aux=null;
        for (Product i : p)
            if(n.compareTo(i.getDenumire())==0){
                aux=i;
                break;
            }
        return aux;
    }
    
    
    /** adaugarea unui nou produs in depozit
	* @param p0
	* @pre prod.getNr()>0
	* @pre prod.getPret()>0
    * @pre prod.getId()>0
    * @post size=size@pre+1;
	*/
	public void produsNou(Product p0)
	{
		assert ((p0.getNr()>0)&&(p0.getPret()>0)&&(p0.getId()>0)) : "Eroare la parametri introdusi\n";
		if(p.add(p0)==false)
		{
			setMesaj("Acest produs exista deja !\n");
		}
		else
		{
			setMesaj("Produsul a fost adaugat !\n");
			size++;
		}
	}
	
	
    /** stergerea unui produs din depozit
	* @param n
	* @pre n!=null
    * @post size=size@p-1;
	*/
	public void stergeProdus(String n)
	{
		assert (n!=null) : "Parametru incorect\n";
		Product p1=findProdus(n);
			if(p.remove(p1)==false)
			{
				setMesaj("Produsul nu exista !");
			}
			else
			{
				setMesaj("Produsul a fost sters !");
				size--;
			}
	}
	
	
    /** adauga o cantitate la cea existenta pentru un produs dat
	* @param nume,cant
	* @pre nume!=null;
	* @pre cant>0
	*/
    public void cresteCantitatea(String nume,int cant){
        assert (nume!=null) && (cant>0) : "Eroare la parametri introdusi\n";
        Product p0=null;
        p0=findProdus(nume);
        if(p0==null){
            setMesaj("Produsul nu exista\n");
        }
        else p0.add(cant);
    }
    
    
    /** scade o cantitate din cea existenta pentru un produs dat
	* @param nume,cant
	* @pre nume!=null;
	* @pre cant>0
	*/
    public void scadeCantitatea(String nume,int cant){
        assert (nume!=null) && (cant>0) : "Eroare la parametri introdusi\n";
        Product p0=null;
        p0=findProdus(nume);
        if(p0==null){
            setMesaj("Produsul nu exista\n");
        }
        else p0.minus(cant);
    }
    
    
    /** returneaza true daca este depasit stocul si false in caz contrar
    * stoc maxim = 1000
	* @param n
	* @pre n!=null;
	* @post @nochange
	*/
    public boolean supraStoc(String n)
    {
    	assert n!=null: "Prametri gresiti\n";
    	Product p0=findProdus(n);
    	boolean ok=false;
    	if(p0==null)
    		setMesaj("Produsul nu exista !\n");
    	else
    	{
    		if(p0.getNr()>1000)
    			ok=true;
    		else
    			ok=false;
    	}
    	return ok;
    }
    
    
    /** returneaza true daca sunt mai putin de 50 de unitati in stoc
    * si false in caz contrar
	* @param n
	* @pre n!=null;
	* @post @nochange
	*/
    public boolean subStoc(String n)
    {
    	assert n!=null: "Parametri gresiti\n";
    	Product p0=findProdus(n);
    	boolean ok=false;
    	if(p0==null)
    		setMesaj("Produsul nu exista !\n");
    	else
    	{
    		if(p0.getNr()<50)
    			ok=true;
    		else
    			ok=false;
    	}
    	return ok;
    }
}
