package t3_pack;


import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.jgoodies.forms.factories.DefaultComponentFactory;



public class Interfata extends JFrame implements ActionListener, KeyListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Magazin lidl=new Magazin();
	public JPanel panou = new JPanel();
	
	public  JTextArea txtNrClienti = new JTextArea();
	public  JTextArea textArrMin = new JTextArea();
	public  JTextArea textServMin = new JTextArea();
	public  JTextArea textNrCase = new JTextArea();
	public  JTextArea textArrMaxim = new JTextArea();
	public  JTextArea textTimpServMaxim = new JTextArea();
	public  JTextArea textTimpMediu = new JTextArea();
	public  JButton btnStart = new JButton("START");
	
	public  JLabel lblNrClienti = DefaultComponentFactory.getInstance().createLabel("Numar clienti");
	public  JLabel lblTimeArrMin = DefaultComponentFactory.getInstance().createLabel("De la ora");
	public  JLabel lblTimeServMin = DefaultComponentFactory.getInstance().createLabel("Timp servire minim");
	public  JLabel lblNrCase = DefaultComponentFactory.getInstance().createLabel("Numar case");
	public  JLabel lblTimeArrMax = DefaultComponentFactory.getInstance().createLabel("Pana la ora");
	public  JLabel lblTimpServireMaxim = DefaultComponentFactory.getInstance().createLabel("Timp Servire Maxim");
	public  JLabel lblTimpMediu = DefaultComponentFactory.getInstance().createLabel("Timp Mediu");
	
	
	JTextArea textArea = new JTextArea();
	public  JScrollPane scroll = new JScrollPane(textArea);
	
	public void interfata()
	{
		panou=new JPanel();
		panou.setLayout(null);
		panou.setBounds(100, 100, 600, 760);
		
		textArea.setBounds(0, 191, 600, 740);
		txtNrClienti.setBounds(29, 32, 114, 31);
		textArrMin.setBounds(182, 32, 114, 31);
		textServMin.setBounds(325, 32, 114, 31);
		textNrCase.setBounds(29, 89, 114, 31);
		textArrMaxim.setBounds(182, 89, 114, 31);
		textTimpServMaxim.setBounds(325, 89, 114, 31);
		lblNrClienti.setBounds(29, 17, 92, 14);
		lblTimeArrMin.setBounds(185, 17, 92, 14);
		lblTimeServMin.setBounds(325, 17, 114, 14);
		lblNrCase.setBounds(29, 74, 92, 14);
		lblTimeArrMax.setBounds(182, 74, 92, 14);
		lblTimpServireMaxim.setBounds(325, 74, 114, 14);
		btnStart.setBounds(464, 32, 92, 78);
		textTimpMediu.setBounds(464, 146, 92, 34);
		lblTimpMediu.setBounds(464, 132, 92, 14);
		
		
		
		Font label=new java.awt.Font("Cambria", Font.PLAIN, 13);
		Font textIntrodus=new java.awt.Font("MS Reference Sans Serif", Font.BOLD, 18);
		Font butonStart=new java.awt.Font("KaiTi", Font.BOLD, 20);
		Font textAreaf=new java.awt.Font("Arial Narrow", Font.PLAIN, 14);
		
		
		textArea.setFont(textAreaf);
		txtNrClienti.setFont(textIntrodus);
		textArrMin.setFont(textIntrodus);
		textServMin.setFont(textIntrodus);
		textNrCase.setFont(textIntrodus);
		textArrMaxim.setFont(textIntrodus);
		textTimpServMaxim.setFont(textIntrodus);
		lblNrClienti.setFont(label);
		lblTimeArrMin.setFont(label);
		lblTimeServMin.setFont(label);
		lblNrCase.setFont(label);
		lblTimeArrMax.setFont(label);
		lblTimpServireMaxim.setFont(label);
		btnStart.setFont(butonStart);
		textTimpMediu.setFont(textIntrodus);
		lblTimpMediu.setFont(label);
		
		
		
		panou.add(txtNrClienti);
		panou.add(textArrMin);
		panou.add(textServMin);
		panou.add(textNrCase);
		panou.add(textArrMaxim);
		panou.add(textTimpServMaxim);
		panou.add(lblNrClienti);
		panou.add(lblTimeArrMin);
		panou.add(lblTimeServMin);
		panou.add(lblNrCase);
		panou.add(lblTimeArrMax);
		panou.add(lblTimpServireMaxim);
		panou.add(btnStart);
		panou.add(textTimpMediu);
		panou.add(lblTimpMediu);
		
		btnStart.addActionListener(this);
		txtNrClienti.addKeyListener(this);
		textArrMin.addKeyListener(this);
		textServMin.addKeyListener(this);
		textNrCase.addKeyListener(this);
		textArrMaxim.addKeyListener(this);
		textTimpServMaxim.addKeyListener(this);
		
		
		textArea.setEditable(false);
		scroll=new JScrollPane(textArea);
		scroll.setBounds(567, 191, 17, 551);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		panou.add(scroll);
		
		panou.add(textArea);
		this.add(panou);
		this.setSize(601, 750);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setVisible(true);
		this.setTitle("Magazin LIDL");
		
		
		lidl.area=textArea;
		lidl.timp=textTimpMediu;
		
	}
	
	public void keyTyped(KeyEvent eveniment) 
	 {
	    
	}
	
	
	public void keyPressed(KeyEvent eveniment)
	{	
		
	}
	
	
	public void keyReleased(KeyEvent eveniment) 
	{	
		
	}
	
    public void actionPerformed( ActionEvent e)
    {
    	int nrCase,nrClienti;
    	try {
			nrClienti=Integer.parseInt(txtNrClienti.getText());
		} catch (NumberFormatException e1) {
			nrClienti=0;
			javax.swing.JOptionPane.showMessageDialog(null,"Magazinul trebuie sa aiba clienti !");
		}
		try {
			nrCase=Integer.parseInt(textNrCase.getText());
		} catch (NumberFormatException e1) {
			nrCase=0;
			javax.swing.JOptionPane.showMessageDialog(null,"Deschideti o casa !");
		}
		int sTimeMin, sTimeMax, aTimeMin, aTimeMax;
		
		try {
			aTimeMax=Integer.parseInt(textArrMaxim.getText());
			aTimeMin=Integer.parseInt(textArrMin.getText());
			sTimeMax=Integer.parseInt(textTimpServMaxim.getText());
			sTimeMin=Integer.parseInt(textServMin.getText());
		} catch (NumberFormatException e1) {
			sTimeMin=0;
			sTimeMax=0;
			aTimeMin=0;
			aTimeMax=0;
			javax.swing.JOptionPane.showMessageDialog(null,"Introduceti datele corect!");
		}
		
		
		textTimpServMaxim.setEnabled(false);
		textArrMaxim.setEnabled(false);
		textNrCase.setEnabled(false);
		textServMin.setEnabled(false);
		textArrMin.setEnabled(false);
		txtNrClienti.setEnabled(false);
		
		lidl.getNrClienti(nrClienti);
		lidl.getNrCase(nrCase);
		lidl.genereazaClienti(sTimeMin, sTimeMax, aTimeMin, aTimeMax);
		lidl.start(); 
	     
     }
     
    
    
    
	 public static void main(String[] args)
	 {
	      Interfata gui=new Interfata();
	      gui.interfata();
	  }

}
