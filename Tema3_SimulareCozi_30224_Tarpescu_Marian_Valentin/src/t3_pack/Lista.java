package t3_pack;

public class Lista {
	public Client client=new Client();
	public Lista next;
	
	public Lista(Client client, Lista next)
	{
		this.next=next;
		this.client=client;
	}
	
	public Lista(Client client)
	{
		this.client=client;
		next=null;
	}

}
