package t3_pack;

public class Client {
	public int id;
	public int timpAparitie;
	public int timpAsteptare;
	public int timpServire;
	
	public Client()
	{
		this.id=0;
		this.timpAparitie=0;
		this.timpServire=0;
		this.timpAsteptare=0;
	}
	
	public Client(int time1, int time2, int id)
	{
		this.id=id;
		this.timpAparitie=time1;
		this.timpServire=time2;
	}
	
	public void setTimpAsteptare(int wait)
	{
		this.timpAsteptare=wait;
	}
	
	public int getTimpAsteptare()
	{
		return this.timpAsteptare;
	}
	
	

}
