package t3_pack;

import java.util.Random;
import javax.swing.JTextArea;
public class Magazin extends Thread {
	
	public JTextArea area=new JTextArea();
	public JTextArea timp=new JTextArea();
	public Client clienti[];
	public Client client1=new Client();
	public Coada caseMagazin[];
	public int nrClienti;
	public int nrCase;
	public int timpMediu;
	
	Random generator=new Random();
	
	public void getNrClienti(int n)
	{
		nrClienti=n;
	}
	
	public void getNrCase(int n)
	{
		nrCase=n;
	}
	
	public void genereazaClienti(int timpMinimS, int timpMaximS, int timpMinimA, int timpMaximA)
	{
		clienti=new Client[nrClienti];
		int i, j, k, timpS, timpA;
		int diferentaS=Math.abs(timpMaximS - timpMinimS);
		int diferentaA=Math.abs(timpMaximA - timpMinimA);
		
		for(i=0;i<nrClienti;i++)
		{
			timpS=generator.nextInt(diferentaS)+timpMinimS;
			timpA=generator.nextInt(diferentaA)+timpMinimA;
			clienti[i]=new Client(timpA, timpS, i);
		}
		
		Client aux=new Client();
		for(j=0; j<nrClienti; j++)
			for(k=j+1; k<nrClienti; k++)
			{
				if(clienti[j].timpAparitie>clienti[k].timpAparitie)
				{
					aux=clienti[j];
					clienti[j]=clienti[k];
					clienti[k]=aux;
				}
					
			}
		
		//System.out.println("Timpul de aparitie a clientilor \n");
		//for(i=0;i<nrClienti;i++)
		//{
		//	System.out.println("Clientul cu numarul "+ i +" a ajuns la timpul: "+clienti[i].timpAparitie+ "\n");
		//	System.out.println();
			
		//}
	}
	
	public int timpulMinim()
	{
		int i=0,j;
		int min=caseMagazin[0].timp;
		for(j=0; j<nrCase;j++)
		{
			if(caseMagazin[j].timp<min)
			{
				min=caseMagazin[j].timp;
				i=j;
			}
				
		}
		return i;
	}
	
	public void run()
	{
		int clientiDeserviti=0,timpInitial= -1, timpP, index,i,j,k;
		caseMagazin=new Coada[nrCase];
		
		for(i=0; i<nrCase; i++)
			caseMagazin[i]=new Coada();
		int [] check=new int[nrClienti];
		
		for(j=0;j<nrClienti;j++)
			check[j]=0;
		timpP=clienti[0].timpAparitie;
		
		while(timpInitial!=0)
		{
			for(k=0;k<nrClienti;k++)
			{
				index=timpulMinim();
				if(timpP>=clienti[k].timpAparitie && check[k]==0)
				{
					caseMagazin[index].push(clienti[k]);
					timpP=caseMagazin[index].a.client.timpAsteptare+caseMagazin[index].w.client.timpAparitie;
					check[k]=1;
					//area.append("Clientul " +clienti[k].id+ " astepta la casa " + index +"\n");
				}
				else
					timpP++;
			}
			for(i=0;i<nrCase;i++)
			{
				if((caseMagazin[i].timp!=0) &&(caseMagazin[i].a.client.timpAsteptare<=timpP))
				{
					client1=caseMagazin[i].pop();
					clientiDeserviti++;
					
					if(clientiDeserviti==0)
						timpMediu=0;
					else
					{
						timpMediu=timpP/clientiDeserviti;
						timp.setText(""+timpMediu+"");
						
					}
					area.append("Clientul "+client1.id+ " care a ajuns la timpul " +client1.timpAparitie+ ", cu timpul de servire "+client1.timpServire+ " a plecat de la casa " +i+ " dupa ce a asteptat "+client1.timpAsteptare+" minute\n");
					
				}
			}
			
			for(i=0;i<nrCase;i++)
			{
				timpInitial=timpInitial+caseMagazin[i].timp;
			}
		}
	}

}
