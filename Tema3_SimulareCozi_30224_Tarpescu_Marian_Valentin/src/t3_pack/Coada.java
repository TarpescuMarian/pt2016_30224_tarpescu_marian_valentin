package t3_pack;

public class Coada {
	Lista a,w;
	public int timp;
	public int n;
	
	public Coada()
	{
		a=null;
		w=null;
		timp=0;
	}
	
	public void push(Client client)
	{
		if(a==null)
			a=w=new Lista(client);
		else
		{
			Lista p=new Lista(client);
			p.next=a;
			a=p;
		}
		
		timp=timp+client.timpServire;
		client.setTimpAsteptare(timp);
		n++;
				
	}
	
	public Client pop()
	{
		Client client=new Client();
		Lista p;
		
		if(a==null)
			return null;
		
		p=a;
		
		if(a.next==null)
		{
			client=a.client;
			a=null;
			timp=0;
			return client;
		}
		
		
		while(p.next != w)
		{
			p=p.next;
		}
		
		client=w.client;
		w=p;
		p.next=null;
		
		timp=timp-client.timpServire;
		n--;
		return client;
	}

}
