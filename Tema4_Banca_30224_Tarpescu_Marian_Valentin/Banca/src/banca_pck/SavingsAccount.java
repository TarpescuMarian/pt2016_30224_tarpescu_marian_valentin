package banca_pck;

import java.io.Serializable;


public class SavingsAccount extends Account implements Serializable
{
	private static final long serialVersionUID = -3935088756249740647L;
	private double InterestRate;
	
	public SavingsAccount(Person Person, String Id, double TotalMoney, double InterestRate)
	{
		this.Person = Person;
		this.Id=Id;
		this.TotalMoney=TotalMoney;
		this.InterestRate=InterestRate;
	}
	
	public void setInterestRate(double InterestRate)
	{
		this.InterestRate = InterestRate;
	}
	public double getInterestRate()
	{
		return this.InterestRate;
	}
	
	public void addInterest()
	{
		this.TotalMoney = this.TotalMoney + this.TotalMoney * this.InterestRate; 
	}
	public String toString()
	{
		return this.Person.toString() + " " + "ID:" + this.Id + " " +"  Money:"+ String.valueOf(this.TotalMoney) + " Savings Account";
	}
}
