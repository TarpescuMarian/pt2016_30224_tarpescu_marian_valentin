package banca_pck;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map.Entry;


public class GUI {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		Bank Bank = new Bank(null);
		HashMap<String, Account> temp = new HashMap<String, Account>();
		Bank.setBank(temp);
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 412);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 11, 414, 351);
		frame.getContentPane().add(tabbedPane);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Client", null, panel_1, null);
		panel_1.setLayout(null);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(10, 113, 389, 199);
		panel_1.add(textPane);
		
		textField = new JTextField();
		textField.setBounds(10, 20, 104, 20);
		panel_1.add(textField);
		textField.setColumns(10);
		
		textField_8 = new JTextField();
		textField_8.setBounds(9, 54, 86, 20);
		panel_1.add(textField_8);
		textField_8.setColumns(10);
		
		JLabel lblCnp = new JLabel("CNP");
		lblCnp.setBounds(49, 5, 46, 14);
		panel_1.add(lblCnp);
		
		JButton btnNewButton = new JButton("Accounts");
		btnNewButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				HashMap<String, Account> temporary = new HashMap<String, Account>();
				String s = new String();
				temporary = Bank.SearchByOwner(textField.getText());
				for (Entry<String, Account> entry : temporary.entrySet())
					s = s + entry.getValue().toString() + '\n';
				textPane.setText(s);
				Bank.Store();
			}
		});
		btnNewButton.setBounds(298, 24, 89, 23);
		panel_1.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Withdraw");
		btnNewButton_1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Bank.Withdraw(Double.parseDouble(textField_7.getText()), textField_8.getText());
				Bank.Store();
			}
		});
		btnNewButton_1.setBounds(298, 53, 89, 23);
		panel_1.add(btnNewButton_1);
		
		JButton btnDeposit = new JButton("Deposit");
		btnDeposit.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				Bank.Deposit(Double.parseDouble(textField_7.getText()), textField_8.getText());
				Bank.Store();
			}
		});
		btnDeposit.setBounds(298, 79, 89, 23);
		panel_1.add(btnDeposit);
		
		JButton btnCloseAccount = new JButton("Close Account");
		btnCloseAccount.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				Bank.closeAccount(textField.getText(), textField_8.getText());
				Bank.Store();
			}
		});
		btnCloseAccount.setBounds(10, 79, 133, 23);
		panel_1.add(btnCloseAccount);
		
		textField_7 = new JTextField();
		textField_7.setBounds(202, 54, 86, 20);
		panel_1.add(textField_7);
		textField_7.setColumns(10);
		
		JLabel lblSum = new JLabel("Sum");
		lblSum.setBounds(224, 28, 46, 14);
		panel_1.add(lblSum);
		
		JLabel lblId = new JLabel("Id");
		lblId.setBounds(35, 41, 46, 14);
		panel_1.add(lblId);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Bank", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblAccountId = new JLabel("Account Id");
		lblAccountId.setBounds(22, 11, 74, 14);
		panel.add(lblAccountId);
		
		JTextPane textPane_1 = new JTextPane();
		textPane_1.setBounds(0, 149, 303, 163);
		panel.add(textPane_1);
		
		textField_1 = new JTextField();
		textField_1.setBounds(10, 36, 86, 20);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(313, 25, 86, 20);
		panel.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(313, 60, 86, 20);
		panel.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(313, 105, 86, 20);
		panel.add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setBounds(313, 149, 86, 20);
		panel.add(textField_5);
		textField_5.setColumns(10);
		
		JLabel lblCnp_1 = new JLabel("CNP");
		lblCnp_1.setBounds(313, 11, 46, 14);
		panel.add(lblCnp_1);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setBounds(313, 46, 86, 14);
		panel.add(lblAddress);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(313, 91, 86, 14);
		panel.add(lblName);
		
		JLabel lblPosition = new JLabel("Position");
		lblPosition.setBounds(313, 135, 63, 14);
		panel.add(lblPosition);
		
		textField_6 = new JTextField();
		textField_6.setBounds(159, 36, 86, 20);
		panel.add(textField_6);
		textField_6.setColumns(10);
		
		JLabel lblAccountType = new JLabel("Account Type");
		lblAccountType.setBounds(172, 11, 96, 14);
		panel.add(lblAccountType);
		
		JButton btnAddAccount = new JButton("Add Account");
		btnAddAccount.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (textField_6.getText().equals("Savings"))
					Bank.AddAccount(new SavingsAccount(new Person(textField_2.getText(), textField_4.getText(), textField_3.getText(), textField_5.getText()), textField_1.getText(), 0, 0.05));
				if (textField_6.getText().equals("Spending"))
					Bank.AddAccount(new SpendingAccount(new Person(textField_2.getText(), textField_4.getText(), textField_3.getText(), textField_5.getText()), textField_1.getText(), 0));
				Bank.Store();
			}
		});
		btnAddAccount.setBounds(0, 67, 134, 23);
		panel.add(btnAddAccount);
		
		JButton btnRemoveAccount = new JButton("Remove Account");
		btnRemoveAccount.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				Bank.RemoveAccount(textField_1.getText());
				Bank.Store();
			}
		});
		btnRemoveAccount.setBounds(144, 115, 159, 23);
		panel.add(btnRemoveAccount);
		
		JButton btnSearchByOwner = new JButton("Search By Owner");
		btnSearchByOwner.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				HashMap<String, Account> temporary = new HashMap<String, Account>();
				String s = new String();
				temporary = Bank.SearchByOwner(textField_2.getText());
				for (Entry<String, Account> entry : temporary.entrySet())
					s = s + entry.getValue().toString() + '\n';
				textPane_1.setText(s);
				Bank.Store();
			}
		});
		btnSearchByOwner.setBounds(144, 67, 159, 23);
		panel.add(btnSearchByOwner);
		
		JButton btnSearchByAccount = new JButton("Search By Account");
		btnSearchByAccount.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				Account temporary = Bank.SearchByAccount(textField_1.getText());
				textPane_1.setText(temporary.toString());
				Bank.Store();
			}
		});
		btnSearchByAccount.setBounds(144, 92, 159, 23);
		panel.add(btnSearchByAccount);
		
		JButton btnTotalAccount = new JButton("Total Accounts");
		btnTotalAccount.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				textPane_1.setText(Bank.NumberOf());
				Bank.Store();
			}
		});
		btnTotalAccount.setBounds(0, 92, 134, 23);
		panel.add(btnTotalAccount);
		
		JButton btnTotalSum = new JButton("Total Sum");
		btnTotalSum.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				textPane_1.setText(String.valueOf(Bank.TotalSum()));
				Bank.Store();
			}
		});
		btnTotalSum.setBounds(0, 115, 134, 23);
		panel.add(btnTotalSum);
		
		JButton btnLoadBank = new JButton("Load Bank");
		btnLoadBank.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				Bank.Load();
			}
		});
		btnLoadBank.setBounds(310, 177, 89, 23);
		panel.add(btnLoadBank);
	}
}

