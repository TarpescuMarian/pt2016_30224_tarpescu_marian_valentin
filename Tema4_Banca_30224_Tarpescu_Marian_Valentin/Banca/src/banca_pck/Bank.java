package banca_pck;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map.Entry;


public class Bank implements Serializable, BankProc
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HashMap<String, Account> Bank;
	
	public Bank(HashMap<String, Account> Bank)
	{
		this.Bank = Bank;
	}
	
	public void setBank(HashMap<String, Account> Bank)
	{
		this.Bank = Bank;
	}
	public HashMap<String, Account> getBank()
	{
		return this.Bank;
	}
	
	public void AddAccount(Account Account)
	{
		assert(Account.getId().equals(null)):"Precondition problem: Id non existent";
		assert(Account.getPerson().getCNP().length()==10):"Precondition problem: CNP incorect";
		int length = this.Bank.size();
		assert(isWellFormed()):"not well formed";
		Bank.put(Account.getId(), Account);
		assert(isWellFormed()):"not well formed";
		assert(this.Bank.size()==length+1): "post condition problem: length doesn't work";
	}
	public void RemoveAccount(String Account)
	{
		assert(Account.equals(null)): "Precondition problem: no transmission";
		int length = this.Bank.size();
		Bank.remove(Account);
		assert(isWellFormed()):"not well formed";
		assert(this.Bank.size()==length-1): "post condition problem: length doesn't work";
	}
	public void closeAccount(String CNP, String Id)
	{
		assert(Id.equals(null)): "Precondition problem: no transmission";
		assert(CNP.equals(null)): "Precondition problem: no transmission";
		int length = this.Bank.size();
		for (Entry<String, Account> entry : this.Bank.entrySet())
			if (entry.getValue().getPerson().getCNP().equals(CNP) && entry.getValue().getId().equals(Id))
			{
				Bank.remove(entry.getKey());
				break;
			}
		assert(isWellFormed()):"not well formed";
		assert(this.Bank.size()==length-1): "post condition problem: length doesn't work";
	}
	public HashMap<String, Account> SearchByOwner(String CNP)
	{
		assert(CNP.equals(null)): "Precondition problem: no transmission";
		assert(isWellFormed()):"not well formed";
		HashMap<String, Account> temporary = new HashMap<String, Account>();
		for (Entry<String, Account> entry : this.Bank.entrySet())
			if (entry.getValue().getPerson().getCNP().equals(CNP))	
				temporary.put(entry.getKey(), entry.getValue());
		assert(isWellFormed()):"not well formed";
		assert(temporary == null):"Nothing Found";
		return temporary;
	}
	public void Withdraw(Double w, String Id)
	{
		assert(Id.equals(null)): "Precondition problem: no transmission";
		assert(w < 0.0): "Precondition problem: Cannot withdram neggative sums";
		assert(isWellFormed()):"not well formed";
		double initial_sum = this.Bank.get(Id).getTotalMoney(), secondary_sum=0;
		if (this.Bank.get(Id) instanceof SpendingAccount)
		{
			((SpendingAccount) this.Bank.get(Id)).withdrawMoney(w);
			((SpendingAccount) this.Bank.get(Id)).withdrawInterest(w);
			secondary_sum = this.Bank.get(Id).getTotalMoney();
		}
		assert(isWellFormed()):"not well formed";
		assert(secondary_sum == initial_sum - w - 0.05*w):"Postcondition problem: sum not changed";
	}
	public void Deposit(Double w, String Id)
	{
		assert(Id.equals(null)): "Precondition problem: no transmission";
		assert(w < 0.0): "Precondition problem: Cannot withdram neggative sums";
		assert(isWellFormed()):"not well formed";
		if (this.Bank.get(Id) instanceof SpendingAccount)
			this.Bank.get(Id).addMoney(w);
		if (this.Bank.get(Id) instanceof SavingsAccount)
		{
			this.Bank.get(Id).addMoney(w);
			((SavingsAccount) this.Bank.get(Id)).addInterest();
		}
		assert(isWellFormed()):"not well formed";
	}
	public Account SearchByAccount(String AccountId)
	{
		assert(AccountId.equals(null)):"Precondition problem: no Id";
		assert(isWellFormed()):"not well formed";
		return Bank.get(AccountId);
	}
	public double TotalSum()
	{
		assert(isWellFormed()):"not well formed";
		double Total = 0;
		for (Entry<String, Account> entry : this.Bank.entrySet())
			Total = Total + entry.getValue().getTotalMoney();
		assert(isWellFormed()):"not well formed";
		assert(Total == 0):"Post condition problem: no change";
		return Total;
	}
	public String NumberOf()
	{
		assert(isWellFormed()):"not well formed";
		int account[] = {0,0};
		for (Entry<String, Account> entry : this.Bank.entrySet())
			if (entry instanceof SpendingAccount)
				account[0]++;
			else
				account[1]++;
		assert(isWellFormed()):"not well formed";
		assert(account[1]!=0 || account[0]!=0):"Post condition problem: bank has at least one account";
		return "Saving Accounts: " + account[1] + "\n" + "Spending Accounts: " + account[0];
	}
	
	public void Store()
	{
		assert(isWellFormed()):"not well formed";
		try
	    {
			FileOutputStream fileOut =
			new FileOutputStream("Bank.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(Bank);
			out.close();
			fileOut.close();
	    }catch(IOException i)
	    {
	        i.printStackTrace();
	    }
		assert(isWellFormed()):"not well formed";
	}
	public void Load()
	{
		assert(isWellFormed()):"not well formed";
		HashMap<String, Account> bank;
		try
	     {
	         FileInputStream fileIn = new FileInputStream("Bank.ser");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         bank = (HashMap<String, Account>) in.readObject();
	         in.close();
	         fileIn.close();
	      }catch(IOException i)
	      {
	         i.printStackTrace();
	         return;
	      }catch(ClassNotFoundException c)
	      {
	         System.out.println("Bank class not found");
	         c.printStackTrace();
	         return;
	      }
		this.Bank = bank;
		assert(isWellFormed()):"not well formed";
	}
	private boolean isWellFormed()
	{
		int lenght = 0;
		for (Entry<String, Account> entry : this.Bank.entrySet())
			lenght++;
		if (this.Bank.size() != lenght)
			return false;
		for (Entry<String, Account> entry : this.Bank.entrySet())
			if (entry.getKey().equals(null))
					return false;
		for (Entry<String, Account> entry : this.Bank.entrySet())
			if (entry.getValue().getId().equals(null))
					return false;
		for (Entry<String, Account> entry : this.Bank.entrySet())
			if (entry.getValue().getPerson().getCNP().length() != 13)
					return false;
		for (Entry<String, Account> entry : this.Bank.entrySet())
			if (entry.getValue().getPerson().getName().equals(null))
					return false;
		for (Entry<String, Account> entry : this.Bank.entrySet())
			if (entry.getValue().getPerson().getAddress().equals(null))
					return false;
		for (Entry<String, Account> entry : this.Bank.entrySet())
			if (entry.getValue().getPerson().getWorkAndPosition().equals(null))
					return false;		
		return true;
	}
}

