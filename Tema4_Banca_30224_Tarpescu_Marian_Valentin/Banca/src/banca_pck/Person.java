package banca_pck;

import java.io.Serializable;


public class Person implements Serializable
{
	private static final long serialVersionUID = -9148881187960174080L;
	private String CNP;
	private String Name;
	private String Address;
	private String WorkAndPosition;
	
	public Person(String CNP, String Name, String Address, String WorkAndPosition)
	{
		this.CNP=CNP;
		this.Name=Name;
		this.Address=Address;
		this.WorkAndPosition=WorkAndPosition;
	}
	
	public void setCNP(String CNP)
	{
		this.CNP = CNP;
	}
	public String getCNP()
	{
		return this.CNP;
	}
	
	public void setName(String Name)
	{
		this.Name=Name;
	}
	public String getName()
	{
		return this.Name;
	}
	
	public void setAddress(String Address)
	{
		this.Address=Address;
	}
	public String getAddress()
	{
		return this.Address;
	}
	
	public void setWorkAndPosition(String WorkAndPosition)
	{
		this.WorkAndPosition=WorkAndPosition;
	}
	public String getWorkAndPosition()
	{
		return this.WorkAndPosition;
	}
	
	public String toString()
	{
		return "CNP:" + this.CNP + " " + "  Name:  " + this.Name + " " + "  Address:" + this.Address + " " + "Work:  " + this.WorkAndPosition+'\n';
	}
}

