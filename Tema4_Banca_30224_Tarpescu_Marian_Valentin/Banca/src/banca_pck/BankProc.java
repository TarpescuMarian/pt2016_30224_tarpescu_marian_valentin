package banca_pck;

import java.util.HashMap;
public interface BankProc 
{	
	/*
	 * @Precondition:we must transmit an account
	 * @isWellFormed:hashmap has not have the correct length and certain parameters cannot be null
	 * @Postcondition:hashmap size has to increase
	 */
	void AddAccount(Account Account);
	
	/*
	 * @Precondition:we must transmit an account id
	 * @isWellFormed:hashmap has not have the correct lenght and certain parameters cannot be null
	 * @Postcondition:hashmap size has to decrease
	 */
	void RemoveAccount(String Account);
	
	/*
	 * @Precondition:we must transmit an account id and a client CNP
	 * @isWellFormed:hashmap has not have the correct lenght and certain parameters cannot be null
	 * @Postcondition:hashmap size has to decrease
	 */
	void closeAccount(String CNP, String Id);
	
	/*
	 * @Precondition:we must transmit a client CNP
	 * @isWellFormed:hashmap has not have the correct lenght and certain parameters cannot be null
	 * @Postcondition:we have to transmit a not null hashmap
	 */
	HashMap<String, Account> SearchByOwner(String CNP);
	
	/*
	 * @Precondition:we must transmit an account id and a sum to withdraw, also the sum has to be greater than 0
	 * @isWellFormed:hashmap has not have the correct lenght and certain parameters cannot be null
	 * @Postcondition:sum in account has to change acordingly
	 */
	void Withdraw(Double w, String Id);
	
	/*
	 * @Precondition:we must transmit an account id and a sum to withdraw, also the sum has to be greater than 0
	 * @isWellFormed:hashmap has not have the correct lenght and certain parameters cannot be null
	 * @Postcondition:sum in account has to change acordingly
	 */
	void Deposit(Double w, String Id);
	
	/*
	 * @Precondition:we must transmit an account id
	 * @isWellFormed:hashmap has not have the correct lenght and certain parameters cannot be null
	 * @Postcondition:sum in account has to change acordingly
	 */
	Account SearchByAccount(String AccountId);
	
	/*
	 * @Precondition:none, it is included in the isWellFormed
	 * @isWellFormed:hashmap has not have the correct lenght and certain parameters cannot be null
	 * @Postcondition:we must have a sum different from 0
	 */
	double TotalSum();
	
	/*
	 * @Precondition:none, included in the isWellFormed
	 * @isWellFormed:hashmap has not have the correct lenght and certain parameters cannot be null
	 * @Postcondition:we have to transmit at least one not 0 parameter
	 */
	String NumberOf();
}

