package banca_pck;

import java.io.Serializable;


public class SpendingAccount extends Account implements Serializable
{
	private static final long serialVersionUID = -1281018374072661395L;

	public SpendingAccount(Person Person, String Id, double TotalMoney)
	{
		this.Person = Person;
		this.Id=Id;
		this.TotalMoney=TotalMoney;
	}
	
	public void withdrawMoney(double Take)
	{
		this.TotalMoney = this.TotalMoney - Take;
	}
	public void withdrawInterest(double Take)
	{
		this.TotalMoney = this.TotalMoney - Take * 0.05;
	}
	
	public String toString()
	{
		return this.Person.toString() + " " + "ID:"+this.Id + " " + "  Money-"+String.valueOf(this.TotalMoney) + " Spending Account";
	}
}
