package banca_pck;

import java.io.Serializable;


public class Account implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7324739587754119746L;
	protected Person Person;
	protected String Id;
	protected double TotalMoney;
	
	public void setPerson(Person Person)
	{
		this.Person=Person;
	}
	public Person getPerson()
	{
		return this.Person;
	}
	
	public void setId(String Id)
	{
		this.Id=Id;
	}
	public String getId()
	{
		return this.Id;
	}
	
	public void setTotalMoney(double TotalMoney)
	{
		this.TotalMoney=TotalMoney;
	}
	public double getTotalMoney()
	{
		return this.TotalMoney;
	}
	public void addMoney(double Add)
	{
		this.TotalMoney = this.TotalMoney + Add;
	}
}

